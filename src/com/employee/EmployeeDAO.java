package com.employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
	public List<Employee> getAllEmployees() throws SQLException {
        List<Employee> employees = new ArrayList<>();
        String query = "SELECT * FROM Employees";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(query);
             ResultSet rs = pstmt.executeQuery()) {
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setId(rs.getInt("id"));
                employee.setFirstName(rs.getString("first_name"));
                employee.setLastName(rs.getString("last_name"));
                employee.setDivision(rs.getString("division"));
                employee.setBuilding(rs.getString("building"));
                employee.setTitle(rs.getString("title"));
                employee.setRoom(rs.getString("room"));
                employees.add(employee);
            }
        }
        return employees;
    }
	
	public Object insertEmployee(Employee employee) throws SQLException {
		String sql = "";
		if (employee.getId() > 0) {
	        updateEmployee(employee);
	    } else {
			sql = "INSERT INTO employees (first_name, last_name, division, building, title, room) VALUES (?, ?, ?, ?, ?, ?)";
	        try (Connection conn = DatabaseConnection.getConnection();
	             PreparedStatement pstmt = conn.prepareStatement(sql)) {
	            pstmt.setString(1, employee.getFirstName());
	            pstmt.setString(2, employee.getLastName());
	            pstmt.setString(3, employee.getDivision());
	            pstmt.setString(4, employee.getBuilding());
	            pstmt.setString(5, employee.getTitle());
	            pstmt.setString(6, employee.getRoom());
	            pstmt.executeUpdate();
	        }
	    }
		return sql;
    }

    public void updateEmployee(Employee employee) throws SQLException {
        String sql = "UPDATE employees SET first_name = ?, last_name = ?, division = ?, building = ?, title = ?, room = ? WHERE id = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, employee.getFirstName());
            pstmt.setString(2, employee.getLastName());
            pstmt.setString(3, employee.getDivision());
            pstmt.setString(4, employee.getBuilding());
            pstmt.setString(5, employee.getTitle());
            pstmt.setString(6, employee.getRoom());
            pstmt.setInt(7, employee.getId());
            pstmt.executeUpdate();
        }
    }

    public Object deleteEmployee(int employeeId) throws SQLException {
        String sql = "DELETE FROM employees WHERE id = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, employeeId);
            pstmt.executeUpdate();
        }
		return sql;
    }

	public Employee fetchEmployeeById(int employeeId) {
		Employee employee = null;
	    String sql = "SELECT * FROM employees WHERE id = ?";
	    try (Connection conn = DatabaseConnection.getConnection();
	         PreparedStatement pstmt = conn.prepareStatement(sql)) {
	        pstmt.setInt(1, employeeId);
	        try (ResultSet rs = pstmt.executeQuery()) {
	            if (rs.next()) {
	                employee = new Employee();
	                employee.setId(rs.getInt("id"));
	                employee.setFirstName(rs.getString("first_name"));
	                employee.setLastName(rs.getString("last_name"));
	                employee.setDivision(rs.getString("division"));
	                employee.setBuilding(rs.getString("building"));
	                employee.setTitle(rs.getString("title"));
	                employee.setRoom(rs.getString("room"));
	            }
	        }
	    } catch (SQLException e) {
			e.printStackTrace();
		}
	    return employee;
	}
}
