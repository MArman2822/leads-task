package com.employee;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="employee")
@RequestScoped
public class Employee {
	private int id;
    private String firstName;
    private String lastName;
    private String division;
    private String building;
    private String title;
    private String room;
    
	public Employee() {
	}

	public Employee(String firstName, String lastName, String division, String building, String title, String room) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.division = division;
		this.building = building;
		this.title = title;
		this.room = room;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}	
}
