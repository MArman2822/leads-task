package com.employee;

import java.sql.SQLException;
import java.util.List;

public class EmployeeService {
	private EmployeeDAO empDao = new EmployeeDAO();
	
	public List<Employee> getAllEmployees() throws SQLException {
        return empDao.getAllEmployees();
    }

    public Object insertEmployee(Employee employee) throws SQLException {
        // Implement method to insert an employee into the database
    	return empDao.insertEmployee(employee);
    }

    public void updateEmployee(Employee employee) {
        // Implement method to update an employee in the database
    }

    public Object deleteEmployee(int employeeId) {
        // Implement method to delete an employee from the database
    	try {
			return empDao.deleteEmployee(employeeId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return employeeId;
    }

	public Employee fetchEmployeeById(int employeeId) {
		// TODO Auto-generated method stub
		return empDao.fetchEmployeeById(employeeId);
	}
}
