package com.employee;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@ManagedBean(name = "employeeBean", eager = true)
@RequestScoped
public class EmployeeBean implements Serializable {
	private List<Employee> employees;
	private EmployeeService employeeService = new EmployeeService();
	private Employee employee = new Employee();
	private int employeeId;
	private int selectedEmployeeId;
	private UploadedFile uploadedFile;

	@PostConstruct
	public void init() {
		refreshEmployees();
	}

	public int getSelectedEmployeeId() {
		return selectedEmployeeId;
	}

	public void setSelectedEmployeeId(int selectedEmployeeId) {
		this.selectedEmployeeId = selectedEmployeeId;
	}

	public String navigateToAddEmployee() {
		return "/view/employee/createEmployee?faces-redirect=true";
	}

	public String navigateToEmployeeList() {
		return "/view/employee/employeeList?faces-redirect=true";
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

	public void insertEmployee() {
		if (employee.getFirstName() == null || employee.getFirstName().isEmpty() || employee.getLastName() == null
				|| employee.getLastName().isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "First Name and Last Name are required."));
			return;
		}

		try {
			employeeService.insertEmployee(employee);
			refreshEmployees();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee inserted successfully!"));
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/view/employee/employeeList.xhtml");
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
					"Failed to insert employee: " + e.getMessage()));
		}
	}

	public void deleteEmployee(int employeeId) throws SQLException {
		employeeService.deleteEmployee(employeeId);
		refreshEmployees();
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee deleted successfully."));
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.redirect(ec.getRequestContextPath() + "/view/employee/employeeList.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void editEmployee(int employeeId) throws SQLException {
		this.employee = employeeService.fetchEmployeeById(employeeId);
	}

	private void refreshEmployees() {
		try {
			employees = employeeService.getAllEmployees();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void searchEmployee() throws SQLException {
		if (selectedEmployeeId != 0) {
			employee = employeeService.fetchEmployeeById(selectedEmployeeId);
//			FacesContext.getCurrentInstance().addMessage(null,
//					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee found."));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Please select an employee to search."));
		}
	}

	public void importXML() {
		System.out.println("here");
	    if (uploadedFile != null) {
	        try {
	            InputStream input = uploadedFile.getInputstream();
	            List<Employee> employees = parseXML(input);
	            for (Employee employee : employees) {
	                try {
						employeeService.insertEmployee(employee);
					} catch (SQLException e) {
						e.printStackTrace();
					}
	            }
	            
	            input.close();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "XML file imported successfully.");
	            FacesContext.getCurrentInstance().addMessage(null, message);
	            
	            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    		try {
	    			ec.redirect(ec.getRequestContextPath() + "/view/employee/employeeList.xhtml");
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}
	        } catch (IOException e) {
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Failed to import XML file: " + e.getMessage());
	            FacesContext.getCurrentInstance().addMessage(null, message);
	        }
	    } else {
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Please select a file to import.");
	        FacesContext.getCurrentInstance().addMessage(null, message);
	    }
	}
	
	public List<Employee> parseXML(InputStream input) {
		List<Employee> employees = new ArrayList<>();
	    try {
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document doc = builder.parse(input);
	        doc.getDocumentElement().normalize();
	        Element root = doc.getDocumentElement();
	        NodeList nodeList = root.getElementsByTagName("employee");
	        
	        for (int i = 0; i < nodeList.getLength(); i++) {
	            Node node = nodeList.item(i);
	            if (node.getNodeType() == Node.ELEMENT_NODE) {
	                Element employeeElement = (Element) node;

	                String firstName = employeeElement.getElementsByTagName("firstname").item(0).getTextContent();
	                String lastName = employeeElement.getElementsByTagName("lastname").item(0).getTextContent();
	                String title = employeeElement.getElementsByTagName("title").item(0).getTextContent();
	                String division = employeeElement.getElementsByTagName("division").item(0).getTextContent();
	                String building = employeeElement.getElementsByTagName("building").item(0).getTextContent();
	                String room = employeeElement.getElementsByTagName("room").item(0).getTextContent();

	                Employee employee = new Employee(firstName, lastName, title, building, division, room);
	                employees.add(employee);
	            }
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return employees;
	}

}
